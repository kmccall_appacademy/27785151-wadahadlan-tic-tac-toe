class ComputerPlayer

  attr_reader :name, :board
  attr_accessor :mark

  def initialize(name)
    @name = name
    @mark = mark
  end

  def display(board)
    @board = board
  end

  def get_move
    possible_moves = []
    (0..2).each do |row|
      (0..2).each do |col|
        pos = [row, col]
        possible_moves << pos if board[pos].nil?
      end
    end
    if !winning_move?(possible_moves).nil?
      winning_move?(possible_moves)
    else
      possible_moves.shuffle.first
    end
  end

  def winning_move?(array)
    array.each do |pos|
      board[pos] = mark
      if board.winner == mark
        board[pos] = nil
        return pos
      end
      board[pos] = nil
    end
    nil
  end
end
