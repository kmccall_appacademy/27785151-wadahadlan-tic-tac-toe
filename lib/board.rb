class Board
  attr_reader :grid, :marks

  def self.blank_grid
    Array.new(3) { Array.new(3) }
  end

  def initialize(grid = Board.blank_grid)
    @grid = grid
    @marks = marks
  end

  def [](pos)
    row, col = pos
    grid[row][col]
  end

  def []=(pos, value)
    row, col = pos
    grid[row][col] = value
  end

  def place_mark(pos, mark)
    self[pos] = mark
  end

  def empty?(pos)
    self[pos].nil?
  end

  def row(sym)
    grid.each do |row|
      if row.compact.length == 3
        return true if row.all? { |mark| mark == sym }
      end
    end
    false
  end

  def column(sym)
    grid.transpose.each do |col|
      if col.compact.length == 3
        return true if col.all? { |mark| mark == sym }
      end
    end
    false
  end

  def diagonal(sym)
    return true if (0..2).collect { |i| grid[i][i] }.all? do |el|
      el == sym
    end
    return true if (0..2).collect { |i| grid.reverse[i][i] }.all? do |el|
      el == sym
    end
    false
  end

  def winner
    return :X if row(:X) || column(:X) || diagonal(:X)
    return :O if row(:O) || column(:O) || diagonal(:O)
    nil
  end

  def any_empty_squares?
    grid.each { |row| row.any? { |el| return true if el == nil } }
    false
  end

  def over?
    if any_empty_squares? && winner == nil #Game still in progress
      false
    elsif !winner.nil? #if there ever is a declared winner
      true
    elsif any_empty_squares? == false && winner == nil #Tie
      true
    end
  end


end
