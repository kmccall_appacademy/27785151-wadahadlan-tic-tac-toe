class HumanPlayer
  attr_reader :name, :board

  def initialize(name)
    @name = name
  end

  def get_move
    puts "where do you wanna make your mark soldier"
    pos = []
    gets.chomp.split('').map { |x| pos << x.to_i if x[/\d/]}
    pos
  end

  def display(board)
    row0 = "0 |"
    (0..2).each do |col|
      if board.empty?([0, col])
        row0 << "   |"
      else
        row0 << " " + board[[0, col]].to_s + " |"
      end
    end
    row1 = "1 |"
    (0..2).each do |col|
      if board.empty?([1, col])
        row0 << "   |"
      else
        row0 << " " + board[[0, col]].to_s + " |"
      end
    end
    row2 = "2 |"
    (0..2).each do |col|
      if board.empty?([2, col])
        row0 << "   |"
      else
        row0 << " " + board[[0, col]].to_s + " |"
      end
    end

    puts "    0   1   2  "
    puts "  |-----------|"
    puts row0
    puts "  |-----------|"
    puts row1
    puts "  |-----------|"
    puts row2
    puts "  |-----------|"
  end
end
